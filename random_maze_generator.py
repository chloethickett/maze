"""Maze generator for maze game."""

import copy
import pprint
import random


class Maze:
    def __init__(self, width=5, height=5, start_position=None, end_position=None, wall_map=None, map_=None):
        """ Has default arguments width=5, height=5, start_position=None (input a list of length 2 giving x, y coord),
            end_position=None (input a list of length 2 giving x, y coord), wall_map=None (input a list of lists),
            map_=None (input a list of lists)."""
        if map_ is None:
            if wall_map is None:
                if start_position is None:
                    start_position = [random.randint(1, width), random.randint(1, height)]
                if end_position is None:
                    end_position = [random.randint(1, width), random.randint(1, height)]
                self.start = start_position
                self.end = end_position

                if start_position[0] < 0:
                    start_position[0] += width + 2
                if start_position[1] < 0:
                    start_position[1] += height + 2
                if end_position[0] < 0:
                    end_position[0] += width + 2
                if end_position[1] < 0:
                    end_position[1] += height + 2

                number_of_walls = random.randrange((width * height) - abs(start_position[0] - end_position[0]) -
                                                   abs(start_position[1] - end_position[1]))
                grid = [(x + 1, y + 1) for x in range(width) for y in range(height)]
                grid.remove((start_position[0], start_position[1]))
                grid.remove((end_position[0], end_position[1]))
                while True:
                    self.map = [['W' if x in (0, width + 1) or y in (0, height + 1) else 'O'
                                 for x in range(width + 2)]
                                for y in range(height + 2)]
                    self.map[start_position[1]][start_position[0]] = 'S'
                    self.map[end_position[1]][end_position[0]] = 'E'
                    wall_positions = random.sample(grid, number_of_walls)
                    for i, position in enumerate(wall_positions):
                        self.map[position[1]][position[0]] = 'W'
                    if self.possible():
                        break
            else:
                self.map = wall_map
                if start_position is None:
                    while True:
                        start_position = [random.randint(1, width), random.randint(1, height)]
                        if self.map[start_position[1]][start_position[0]] == 'O':
                            break

                if end_position is None:
                    while True:
                        end_position = [random.randint(1, width), random.randint(1, height)]
                        if self.map[start_position[1]][start_position[0]] == 'O':
                            break
                self.start = start_position
                self.end = end_position
                self.map[start_position[1]][start_position[0]] = 'S'
                self.map[end_position[1]][end_position[0]] = 'E'

        else:
            for y, y_level in enumerate(map_):
                try:
                    x = y_level.index('S')
                except ValueError:
                    continue
                else:
                    self.start = (x, y)
                    break
            for y, y_level in enumerate(map_):
                try:
                    x = y_level.index('E')
                except ValueError:
                    continue
                else:
                    self.end = (x, y)
                    break
            self.map = map_

    def possible(self):
        visited = copy.deepcopy(self.map)

        def walk(x, y):
            visited[y][x] = 'W'
            directions = [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]
            for xx, yy in directions:
                if visited[yy][xx] == 'W':
                    continue
                if visited[yy][xx] == 'E':
                    return True
                if walk(xx, yy):
                    return True
            return False

        return walk(self.start[0], self.start[1])


maze = Maze(start_position=[1, 1], end_position=[5, 5])
pprint.pprint(maze.map)
