"""This is a maze game!"""

import copy
import random
import pprint


class Maze:
    def __init__(self, width=5, height=5, start_position=None, end_position=None, wall_map=None, map_=None):
        """ Has default arguments width=5, height=5, start_position=None (input a list of length 2 giving x, y coord),
            end_position=None (input a list of length 2 giving x, y coord), wall_map=None (input a list of lists),
            map_=None (input a list of lists)."""
        if map_ is None:
            if wall_map is None:
                if start_position is None:
                    start_position = [random.randint(1, width), random.randint(1, height)]
                if end_position is None:
                    while True:
                        end_position = [random.randint(1, width), random.randint(1, height)]
                        if end_position != start_position:
                            break
                self.start = start_position
                self.end = end_position

                if start_position[0] < 0:
                    start_position[0] += width + 2
                if start_position[1] < 0:
                    start_position[1] += height + 2
                if end_position[0] < 0:
                    end_position[0] += width + 2
                if end_position[1] < 0:
                    end_position[1] += height + 2

                number_of_walls = random.randrange((width * height) - abs(start_position[0] - end_position[0]) -
                                                   abs(start_position[1] - end_position[1]))
                grid = [(x + 1, y + 1) for x in range(width) for y in range(height)]
                grid.remove((start_position[0], start_position[1]))
                grid.remove((end_position[0], end_position[1]))
                while True:
                    self.map = [['W' if x in (0, width + 1) or y in (0, height + 1) else 'O'
                                 for x in range(width + 2)]
                                for y in range(height + 2)]
                    self.map[start_position[1]][start_position[0]] = 'S'
                    self.map[end_position[1]][end_position[0]] = 'E'
                    wall_positions = random.sample(grid, number_of_walls)
                    for i, position in enumerate(wall_positions):
                        self.map[position[1]][position[0]] = 'W'
                    if self.possible():
                        break
            else:
                self.map = wall_map
                if start_position is None:
                    while True:
                        start_position = [random.randint(1, width), random.randint(1, height)]
                        if self.map[start_position[1]][start_position[0]] == 'O':
                            break

                if end_position is None:
                    while True:
                        end_position = [random.randint(1, width), random.randint(1, height)]
                        if self.map[start_position[1]][start_position[0]] == 'O':
                            break
                self.start = start_position
                self.end = end_position
                self.map[start_position[1]][start_position[0]] = 'S'
                self.map[end_position[1]][end_position[0]] = 'E'

        else:
            for y, y_level in enumerate(map_):
                try:
                    x = y_level.index('S')
                except ValueError:
                    continue
                else:
                    self.start = (x, y)
                    break
            for y, y_level in enumerate(map_):
                try:
                    x = y_level.index('E')
                except ValueError:
                    continue
                else:
                    self.end = (x, y)
                    break
            self.map = map_

    def possible(self):
        visited = copy.deepcopy(self.map)

        def walk(x, y):
            visited[y][x] = 'W'
            directions = [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)]
            for xx, yy in directions:
                if visited[yy][xx] == 'W':
                    continue
                if visited[yy][xx] == 'E':
                    return True
                if walk(xx, yy):
                    return True
            return False

        return walk(self.start[0], self.start[1])
        
    def __getitem__(self, item):
        return self.map[item]

    def __delitem__(self, item):
        del self.map[item]

    def __setitem__(self, item, value):
        self.map[item] = value
        

class Player:
    directions = {'N': (0, -1), 'S': (0, 1), 'E': (-1, 0), 'W': (1, 0)}

    def __init__(self, maze):
        self.maze = maze
        self.position = self.maze.start
        
    def move(self, direction):
        for i in range(2):
            self.position[i] += self.directions[direction][i]
        
       
class Game:
    directions = ['North', 'South', 'East', 'West']

    def __init__(self, maze, player):
        self.maze = maze
        self.player = player
        
    def check_directions(self):
        x, y = self.player.position
        return_list = []

        def empty_space(maze_position):
            return maze_position == 'O' or maze_position == 'E' or maze_position == 'S'
        if empty_space(self.maze[y - 1][x]):
            return_list.append('North')
        if empty_space(self.maze[y + 1][x]):
            return_list.append('South')
        if empty_space(self.maze[y][x - 1]):
            return_list.append('East')
        if empty_space(self.maze[y][x + 1]):
            return_list.append('West')
        return return_list

    @staticmethod
    def display_directions(available_directions):
        if len(available_directions) == 0:
            print("You can't move anywhere.")
        elif len(available_directions) == 1:
            print("You can move " + available_directions[0] + ".")
        else:
            print("You can move " + ', '.join(available_directions[:-1]) + " and " + available_directions[-1] + ".")

    def tick(self):
        x, y = self.player.position
        
        if self.maze[y][x] == 'E':
            print('Well Done! You reached the end!')
            return True
        
        if maze[y][x] == 'W':
            print('How did you get there?')
            
        available_directions = self.check_directions()
        self.display_directions(available_directions)
        inpt = input().casefold().strip()
        if inpt in ('north', 'n', 'move north', 'go north'):
            if 'North' in available_directions:
                self.player.move('N')
            else:
                print("\nYou can't move North at the moment.\n")
        elif inpt in ('south', 's', 'move south', 'go south'):
            if 'South' in available_directions:
                self.player.move('S')
            else:
                print("\nYou can't move South at the moment.\n")
        elif inpt in ('east', 'e', 'move east', 'go east'):
            if 'East' in available_directions:
                self.player.move('E')
            else:
                print("\nYou can't move East at the moment.\n")
        elif inpt in ('west', 'w', 'move west', 'go west'):
            if 'West' in available_directions:
                self.player.move('W')
            else:
                print("\nYou can't move West at the moment.\n")
        else:
            print("\nThat wasn't a valid input.\nType n to go North, s to go South,\ne to go East and w to go West\n")
        return False

    def run(self):
        while True:
            if self.tick():
                break


maze_ = [['W', 'W', 'W', 'W', 'W', 'W', 'W'],
         ['W', 'S', 'O', 'O', 'W', 'O', 'W'],
         ['W', 'W', 'W', 'O', 'W', 'O', 'W'],
         ['W', 'E', 'W', 'O', 'O', 'O', 'W'],
         ['W', 'O', 'W', 'O', 'O', 'W', 'W'],
         ['W', 'O', 'O', 'O', 'O', 'O', 'W'],
         ['W', 'W', 'W', 'W', 'W', 'W', 'W']]


if __name__ == "__main__":
    maze = Maze()
    # pprint.pprint(maze.map)
    player = Player(maze)
    game = Game(maze, player)
    game.run()
